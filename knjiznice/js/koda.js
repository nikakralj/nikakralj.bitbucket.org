var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  $.ajaxSetup({
    headers: {
      "Authorization": getAuthorization()
    }
  });
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function(data) {
      var ehrId = data.ehrId;
      $("#header").html("EHR: " + ehrId);

      //definiram podatke glede na stPacienta
      var partyData;
      var vitalniData;
      if (stPacienta == 1) {
        partyData = {
          firstNames: "Mojca",
          lastNames: "Koron",
          dateOfBirth: new Date("1995-12-12T19:30"),
          partyAdditionalInfo: [{
            key: "ehrId",
            value: ehrId
          }]
        };

        data = {
          ehrId: ehrId,
          telesnaVisina: 165,
          telesnaTeza: 85,
          telesnaTemperatura: 36.5,
          sistolicniKrvniTlak: 125,
          diastolicniKrvniTlak: 96,
          nasicenostKrviSKisikom: 96,
          merilec: "dr. Irena Novak"
        }
      }
      else if (stPacienta == 2) {
        partyData = {
          firstNames: "Jure",
          lastNames: "Novak",
          dateOfBirth: new Date("1965-05-05T19:30"),
          partyAdditionalInfo: [{
            key: "ehrId",
            value: ehrId
          }]
        };
        data = {
          ehrId: ehrId,
          telesnaVisina: 190,
          telesnaTeza: 91,
          telesnaTemperatura: 36.2,
          sistolicniKrvniTlak: 112,
          diastolicniKrvniTlak: 89,
          nasicenostKrviSKisikom: 99,
          merilec: "dr. Jože Klavir"
        }
      }
      else if (stPacienta == 3) {
        partyData = {
          firstNames: "Ema",
          lastNames: "Kralj",
          dateOfBirth: new Date("2005-10-10T19:30"),
          partyAdditionalInfo: [{
            key: "ehrId",
            value: ehrId
          }]
        };
        data = {
          ehrId: ehrId,
          telesnaVisina: 145,
          telesnaTeza: 40,
          telesnaTemperatura: 36.7,
          sistolicniKrvniTlak: 119,
          diastolicniKrvniTlak: 93,
          nasicenostKrviSKisikom: 98,
          merilec: "dr. Klara Juretič"
        }
      }

      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function(party) {
          postVitalniData(data, function() {
            if (party.action == 'CREATE') {
              console.log(party);
              $("#result").html("Created: " + party.meta.href);
              vstaviOpcijoPredloga(partyData.firstNames + " " + partyData.lastNames, ehrId);
              vstaviOpcijoVitalnihZnakov(partyData.firstNames + " " + partyData.lastNames, ehrId);
            }
          });
        }
      });
    }
  });
}

//funkcija ti zgenerira vse tri osebe naenkrat (osebo 1, 2 in 3)
function zgenerirajVsePodatke() {
  generirajPodatke(1);
  generirajPodatke(2);
  generirajPodatke(3);

}
//izpolni prostorčke, ko izberemo osebo iz menija
function vstaviOpcijoPredloga(ime, ehrId) {
  var option = document.createElement("option");
  option.text = ime;
  option.value = ehrId;
  document.getElementById("preberiPredlogoBolnika").appendChild(option);
}

function vstaviOpcijoVitalnihZnakov(ime, ehrId) {
  var option = document.createElement("option");
  option.text = ime;
  option.value = ehrId;
  document.getElementById("preberiObstojeciVitalniZnak").appendChild(option);
}
//-------------------------------------------------------------------------
//Glavne funkcijonalnosti
//------------------------------------------------------------------------

function izracunajBMI(teza, visina) {
  var BMI = teza / (visina * visina) * 10000;
  if (isNaN(BMI)) {
    return undefined;
  }
  return BMI;
}

function prikaziBMI() {
  var visina = $("#dodajVitalnoTelesnaVisina").val();
  var teza = $("#dodajVitalnoTelesnaTeza").val();
  var BMI = izracunajBMI(teza, visina);
  if (BMI == undefined) {
    alert("Najprej vnesi svojo višino in težo.");
    return;
  }
  var formatirano = parseFloat(Math.round(BMI * 100) / 100).toFixed(2);
  $("#izracunajBMI").html("<br> Tvoj BMI znaša: <b>" + formatirano + "</b>");
  narisiGraf($('#bmiGraf'), formatirano);
}

function izracunajStUrAktivnosti() {
  var visina = $("#dodajVitalnoTelesnaVisina").val();
  var teza = $("#dodajVitalnoTelesnaTeza").val();
  var BMI = izracunajBMI(teza, visina);
  if (BMI == undefined) {
    alert("Najprej vnesi svojo višino in težo.");
    return;
  }
  var datumRojstva = $("#kreirajDatumRojstva").val();
  //console.log(izracunajStarost(new Date(datumRojstva)));
  var stLet = izracunajStarost(new Date(datumRojstva));
  var svetovano = undefined;
  if (stLet < 18) {
    if (BMI < 25.0) {
      svetovano = 3;
    }
    else {
      svetovano = 7;
    }
  }
  else if (stLet <= 35) {
    if (BMI < 25.0) {
      svetovano = 4;
    }
    else {
      svetovano = 12;
    }
  }
  else {
    if (BMI < 25.0) {
      svetovano = 2;
    }
    else {
      svetovano = 5;
    }
  }
  //izpiši sporočilo
  $("#izracunajStUrAktivnosti").html("<br> Priporočena aktivnost glede na vaš BMI in starost je: <b>" + svetovano + "h/teden</b>");
}

function izracunajStarost(rojstniDatum) {
  var razlikaMs = Date.now() - rojstniDatum.getTime();
  var datum = new Date(razlikaMs);
  return Math.abs(datum.getUTCFullYear() - 1970);
}

//============================
//    KODA ZA NARISAT GRAF
//============================
//https://www.chartjs.org/docs/latest/
var curChart = undefined;

function narisiGraf(ctx, osebaBmi = undefined) {
  // default vrednosti:
  var labels = ['Prenizka teža', 'Normalna teža', "Prevelika teža", "Debelost"];
  var data = [18, 22, 25, 30]; //mejne/povprečne vrednosti
  var backgroundColor = [
    'rgba(255, 99, 132, 0.2)', // red
    'rgba(75, 192, 192, 0.2)', // green
    'rgba(255, 99, 132, 0.2)', // red
    'rgba(255, 99, 132, 0.2)' // red
  ];
  var borderColor = [
    'rgba(255, 99, 132, 1)', //red
    'rgba(75, 192, 192, 1)', //green
    'rgba(255, 99, 132, 1)', //red
    'rgba(255, 99, 132, 1)' //red
  ];

  // Pripravi podatke
  if (osebaBmi != undefined) {
    labels.push("Vaš BMI");
    data.push(osebaBmi);
    if (osebaBmi > 18.5 && osebaBmi < 24.9) {
      backgroundColor.push('rgba(75, 192, 192, 0.2)'); //green
      borderColor.push('rgba(75, 192, 192, 1)');
    }
    else {
      backgroundColor.push('rgba(255, 99, 132, 0.2)'); //red
      borderColor.push('rgba(255, 99, 132, 1)');
    }
  }


  if (curChart != undefined) {
    // pobrisi star graf
    curChart.destroy();
  }
  curChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
      labels: labels,
      datasets: [{
        label: "Vrednost BMI",
        data: data,
        backgroundColor: backgroundColor,
        borderColor: borderColor,
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }],
        xAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
}

//=====================================================
// KODA ZA ZEMLJEVID
//====================================================
var curMap = undefined;

function narisiZemljevid() {
  // center of the map
  var center = [46.053613, 14.627335];

  // Create the map
  curMap = L.map('zemljevid').setView(center, 8);

  // Set up the OSM layer
  L.tileLayer(
    'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Data © <a href="http://osm.org/copyright">OpenStreetMap</a>',
      maxZoom: 18
    }).addTo(curMap);

  curMap.on('click', function(e) {
    var latlng = e.latlng;
    recolour(latlng);
  });

  $.ajax({
    'async': true,
    'global': false,
    'url': "knjiznice/json/bolnisnice.json",
    'dataType': "json",
    'success': function(data) {
      narisiPoligone(data);
    }
  });
}

function getIcon(colour) {
  return new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-icon-2x-' +
      colour + '.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
}

var polygons = [];
var markers = [];

function narisiPoligone(data) {
  var bolnisnice = data.features;
  

  for (var i in bolnisnice) {
    var opis = `<p>Tip objekta: ${bolnisnice[i].properties.amenity}.<br>Ime objekta: ${bolnisnice[i].properties.name}<br><br>
    <a target="blank" href="https://www.google.com/search?q=${encodeURIComponent(bolnisnice[i].properties.name)}">Več info</a>
    </p>`;

    try {
      if (bolnisnice[i].geometry.type == "Point") {
        // add marker
        var koordinate = bolnisnice[i].geometry.coordinates;
        var marker = L.marker([koordinate[1], koordinate[0]], { icon: getIcon("blue") }).addTo(curMap);
        marker.bindPopup(opis).openPopup;
        markers.push({ marker: marker, latlng: marker.getLatLng() });
      }
      else {
        var koordinate = bolnisnice[i].geometry.coordinates;
        koordinate = prezrcali(koordinate);
        // preracunaj razdaljo in določi barvo
        var mapPoligon = L.polygon(koordinate, { color: 'blue' }).addTo(curMap);
        var popup = mapPoligon.bindPopup(opis).openPopup;
        polygons.push({ polygon: mapPoligon, latlng: mapPoligon.getLatLngs()[0] })
      }
    }
    catch (err) {
      console.log(err);
    }
  }
}

function prezrcali(koordinate) {
  koordinate[0] = koordinate[0].map(function(par) { return [par[1], par[0]] });
  if (koordinate[1] != undefined) {
    koordinate[1] = koordinate[1].map(function(par) { return [par[1], par[0]] });
  }
  return koordinate;
}

const DIST_LIMIT = 50;

function recolour(latlng) {
  
  for (var i in polygons) {
    var p = polygons[i].polygon;
    var color = undefined;
    if (dist(latlng.lat, latlng.lng, polygons[i].latlng.lat, polygons[i].latlng.lng) < DIST_LIMIT) {
      color = "green";
    }
    else {
      color = "blue";
    }
    p.setStyle({fillcolor: color});
  }
  
  for (var i in markers) {
    if (dist(latlng.lat, latlng.lng, markers[i].latlng.lat, markers[i].latlng.lng) < DIST_LIMIT) {
      // recolour point
      markers[i].marker.setIcon(getIcon("green"));
    }
    else {
      markers[i].marker.setIcon(getIcon("blue"));
    }
  }
}


function dist(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2); 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

//==============================================================
//EHR API
//================================================================

function kreirajEHRzaBolnika() {
  var ime = $("#kreirajIme").val();
  var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();

  if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
    priimek.trim().length == 0) {
    $("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite vse podatke!</span>");
  }
  else {
    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function(data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: { "ehrId": ehrId }
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function(party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR:<br>'" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
              vstaviOpcijoPredloga(partyData.firstNames + " " + partyData.lastNames, ehrId);
              vstaviOpcijoVitalnihZnakov(partyData.firstNames + " " + partyData.lastNames, ehrId);
            }
          },
          error: function(err) {
            $("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
    });
  }
}

function isNumeric(string) {
  return !isNaN(Number(string));
}

function dodajMeritveVitalnihZnakov() {
  var ehrId = $("#dodajVitalnoEHR").val();
  var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
  var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
  var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
  var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
  var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
  var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
  var merilec = $("#dodajVitalnoMerilec").val();

  if (!(isNumeric(telesnaVisina) &&
      isNumeric(telesnaTeza) &&
      isNumeric(telesnaTemperatura) &&
      isNumeric(sistolicniKrvniTlak) &&
      isNumeric(diastolicniKrvniTlak) &&
      isNumeric(nasicenostKrviSKisikom))) {
    $("#dodajMeritveVitalnihZnakovSporocilo").html("Podatki morajo biti številka.");
    return;
  }
  if (merilec.trim().length == 0) {
    $("#dodajMeritveVitalnihZnakovSporocilo").html("Vnesite svoje ime kot merilec.");
    return;
  }

  if (!ehrId || ehrId.trim().length == 0) {
    $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    return;
  }

  var data = {
    ehrId: ehrId,
    telesnaVisina: telesnaVisina,
    telesnaTeza: telesnaTeza,
    telesnaTemperatura: telesnaTemperatura,
    sistolicniKrvniTlak: sistolicniKrvniTlak,
    diastolicniKrvniTlak: diastolicniKrvniTlak,
    nasicenostKrviSKisikom: nasicenostKrviSKisikom,
    merilec: merilec
  }
  postVitalniData(data, function() {
    $("#dodajMeritveVitalnihZnakovSporocilo").html(
      "<span class='obvestilo label label-success fade-in'>Uspešno dodano.</span>");
  });

}


function postVitalniData(data, callback) {
  var podatki = {
    // Struktura predloge je na voljo na naslednjem spletnem naslovu:
    // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
    "ctx/language": "en",
    "ctx/territory": "SI",
    "vital_signs/height_length/any_event/body_height_length": data.telesnaVisina,
    "vital_signs/body_weight/any_event/body_weight": data.telesnaTeza,
    "vital_signs/body_temperature/any_event/temperature|magnitude": data.telesnaTemperatura,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/blood_pressure/any_event/systolic": data.sistolicniKrvniTlak,
    "vital_signs/blood_pressure/any_event/diastolic": data.diastolicniKrvniTlak,
    "vital_signs/indirect_oximetry:0/spo2|numerator": data.nasicenostKrviSKisikom
  };
  var parametriZahteve = {
    ehrId: data.ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT',
    committer: data.merilec
  };
  $.ajax({
    url: baseUrl + "/composition?" + $.param(parametriZahteve),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(podatki),
    headers: {
      "Authorization": getAuthorization()
    },
    success: function(res) {
      callback(res);
    },
    error: function(err) {
      $("#dodajMeritveVitalnihZnakovSporocilo").html(
        "<span class='obvestilo label label-danger fade-in'>Napaka '" +
        JSON.parse(err.responseText).userMessage + "'!");
    }
  });
}

function pridobiPredlogo(ehrId, callback) { // callback je funkcija. Pokličeš kot callback();
  $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function(data) {
      var party = data.party;

      var podatki = {
        ime: party.firstNames,
        priimek: party.lastNames,
        rojDatum: party.dateOfBirth
      }
      callback(podatki);
    },
    error: function(err) {
      $("#preberiSporocilo").html("<span class='obvestilo label " +
        "label-danger fade-in'>Napaka '" +
        JSON.parse(err.responseText).userMessage + "'!");
    }
  });
}

$(document).ready(function() {

  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var ehrId = $(this).val();

    // kontaktrii server
    pridobiPredlogo(ehrId, function(podatki) {
      $("#kreirajIme").val(podatki.ime);
      $("#kreirajPriimek").val(podatki.priimek);
      $("#kreirajDatumRojstva").val(podatki.rojDatum);
    });
  });

  $('#preberiObstojeciVitalniZnak').change(function() {
    $("#dodajMeritveVitalnihZnakovSporocilo").html("");
    var ehrId = $(this).val();

    pridobiVitalnalneZnake(ehrId, function(podatki) {
      $("#dodajVitalnoEHR").val(podatki.ehrId);
      $("#dodajVitalnoTelesnaVisina").val(podatki.visina);
      $("#dodajVitalnoTelesnaTeza").val(podatki.teza);
      $("#dodajVitalnoTelesnaTemperatura").val(podatki.temperatura);
      $("#dodajVitalnoKrvniTlakSistolicni").val(podatki.tlakS);
      $("#dodajVitalnoKrvniTlakDiastolicni").val(podatki.tlakD);
      $("#dodajVitalnoNasicenostKrviSKisikom").val(podatki.kisikVKrvi);
      $("#dodajVitalnoMerilec").val("");
    });
  });

});

function pridobiVitalnalneZnake(ehrId, callback) {
  new Promise(async function(resolve, reject) {
    var podatki = {
      ehrId: ehrId
    };

    await $.ajax({
      url: baseUrl + "/view/" + ehrId + "/" + "height",
      type: 'GET',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function(data) {
        podatki.visina = (data.length > 0) ? data[0].height : "";
      }
    });

    await $.ajax({
      url: baseUrl + "/view/" + ehrId + "/" + "weight",
      type: 'GET',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function(data) {
        podatki.teza = (data.length > 0) ? data[0].weight : "";
      }
    });

    await $.ajax({
      url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
      type: 'GET',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function(data) {
        //data je array objektov. vsak je temperatura + čas
        podatki.temperatura = (data.length > 0) ? data[0].temperature : "";
      }
    }); // konec ajaxa

    await $.ajax({
      url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
      type: 'GET',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function(data) {
        podatki.tlakS = (data.length > 0) ? data[0].systolic : "";
        podatki.tlakD = (data.length > 0) ? data[0].diastolic : "";
      }
    });

    await $.ajax({
      url: baseUrl + "/view/" + ehrId + "/spO2",
      type: 'GET',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function(data) {
        podatki.kisikVKrvi = (data.length > 0) ? data[0].spO2 : "";
      }
    });

    callback(podatki);

  });
}